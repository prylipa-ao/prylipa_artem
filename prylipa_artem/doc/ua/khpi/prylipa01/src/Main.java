/**
* Print comand-line parameters.
*/
public class Main {
  /**
  * Program entry point.
  * @param args comand-line parameters list
  */
  public static void main(String[] args)  {

     for(int i = 0; i < args.length; i++){
        for (int j = 0; j < i; j++) {
            if (args[j].charAt(0) > args[j+1].charAt(0)) {
                String max = args[j];
                args[j] = args[j + 1];
                args[j + 1] = max; }
            }
     }
     for(String s: args) {
        System.out.println(s);
      }
   }
}
