/*
 * @file contains implementation MainTest
 */
package ua.khpi.prylipa02;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import java.io.IOException;
import ua.khpi.prylipa02.Calc;


/**
 * The Class MainTest tested main function of program.
 */
public class MainTest {

	/**
	 * Test calc.
	 */
	@Test
	public void testCalc() {
		Calc calc = new Calc();
		calc.init(1, 0);
		calc.calc();
		assertEquals(0, calc.getResult());
		
		calc.init(10, 1);
		calc.calc();
		assertEquals(2, calc.getResult());
	
		calc.init(6, 8);
		calc.calc();
		assertEquals(3, calc.getResult());

	}

	/**
	 * Test restore.
	 */
	@Test
	public void testRestore() {
		Calc calc = new Calc();
		double weight = 6, height = 8;
	
			calc.init(weight, height);
			calc.calc();
			try {
				calc.save();
			} catch (IOException e1) {
				System.out.println("Serialization error: " + e1);
			}

			calc.setResult(0);
			try {
				calc.restore();
			} catch (Exception e) {
				System.out.println("Serialization error: " + e);
			}

			assertEquals(3, calc.getResult());

		
	}
}