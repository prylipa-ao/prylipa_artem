package ua.khpi.prylipa06;

import static org.junit.jupiter.api.Assertions.*;
import java.util.concurrent.TimeUnit;
import org.junit.jupiter.api.Test;

import ua.khpi.prylipa03.ViewResult;

class MainTest {

	ViewResult view = new ViewResult();
	AvgCommand avg = new AvgCommand(view);
	MinCommand min = new MinCommand(view);
	MaxCommand max = new MaxCommand(view);

	@Test
	void testAvg() {
		view.init();
		avg.execute();
		assertTrue(avg.getResult() > 0.0);
	}

	@Test
	void testMin() {
		view.init();
		min.execute();
		assertTrue(min.getResult() > -1);
	}

	@Test
	void testMax() {
		view.init();
		max.execute();
		assertTrue(max.getResult() > -1);
	}

	@Test
	void testQueue() {
		CommandQueue queue = new CommandQueue();
		queue.add(avg);
		queue.add(min);
		queue.add(max);
		while (!avg.finish() && !min.finish() && !max.finish())
			try {

				TimeUnit.MILLISECONDS.sleep(100);
			} catch (InterruptedException e) {
				fail("Error run queue");
			}
	}

}
