/*
 * @file contains implementation MainTest
 */
package ua.khpi.prylipa03;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.jupiter.api.Test;

/**
 * The Class MainTest tested main function of program.
 */
class MainTest {

	/**
	 * Test calc.
	 */
	@Test
	public void testCalc() {
		ViewResult view = new ViewResult();
		view.init(1, 1);

		assertEquals(1, view.getItems().get(0).getResult());
		assertEquals(1, view.getItems().get(1).getResult());
		assertEquals(1, view.getItems().get(2).getResult());
		assertEquals(1, view.getItems().get(3).getResult());
		assertEquals(1, view.getItems().get(4).getResult());
	}

	/**
	 * Test restore.
	 */
	@Test
	public void testRestore() {
		ViewResult view = new ViewResult();
		view.init(1, 1);
		for (int i = 0; i < 5; i++) {
			try {
				view.viewSave();
			} catch (IOException e1) {
				System.out.println("Serialization error: " + e1);
			}

			view.getItems().get(i).setResult(0);
			try {
				view.viewRestore();
			} catch (Exception e) {
				System.out.println("Serialization error: " + e);
			}

			assertEquals(1, view.getItems().get(i).getResult());
		}
	}
}
