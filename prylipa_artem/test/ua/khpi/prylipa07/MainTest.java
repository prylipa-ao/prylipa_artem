package ua.khpi.prylipa07;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class MainTest {

	static Collection collection = new Collection();
	static InitEventHandler ih = new InitEventHandler();
	static EventHanlders eh = new EventHanlders();
	static Data expected = new Data("expected");

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		collection.addObserver(ih);
		collection.addObserver(eh);
		collection.add(new Data("A"));
		collection.add(expected);
		collection.add(new Data(""));
		collection.add(expected);
	}

	@Test
	void testSort() {
		List<Data> tmp = new ArrayList<Data>(collection.getCollection());
		Collections.sort(collection.getCollection());
		assertEquals(tmp, collection.getCollection());
	}

	@Test
	void testSearch() {
		String expected = "A";
		collection.call("ITEM_SEARCHMIN");
		assertEquals(expected, collection.getCollection().get(0).toString());
	}

	@Test
	void testAdd() {
		for (Data actual : collection) {
			assertFalse(actual.getData().isEmpty());
		}
		assertEquals(4, collection.getCollection().size());
	}

	@Test
	void testDelete() {
		collection.delete(3);
		collection.delete(2);
		assertEquals(2, collection.getCollection().size());
	}

}
