package ua.khpi.prylipa08;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.Test;

import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.stage.Stage;
import ua.khpi.prylipacp.Cell;
import ua.khpi.prylipacp.Game;
import ua.khpi.prylipacp.GameManager;
import ua.khpi.prylipacp.Main;

class MainTest {

	@Test
	public void testRun() throws InterruptedException {
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				new JFXPanel();
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						new Main().start(new Stage());

					}
				});
			}
		});
		thread.start();
		Thread.sleep(1000);
	}

	@Test
	public void testSetValue() {
		GameManager gm = new GameManager();
		Game.cells[Game.COLUMNS - 1][Game.ROWS - 1] = new Cell(false);
		for (int r = 0; r < Game.ROWS; r++) {
			for (int c = 0; c < Game.COLUMNS; c++) {
				Game.cells[r][c].setValue(2);
				assertEquals(2, Game.cells[r][c].getValue());
			}
		}

	}

	@Test
	public void testSwapUp() {
		GameManager gm = new GameManager();
		Game.cells[Game.COLUMNS - 1][Game.ROWS - 1] = new Cell(false);
		Game.cells[0][0].setValue(2);
		Game.cells[0][1].setValue(2);
		gm.game.swapUp();
		assertEquals(4, Game.cells[0][0].getValue());

	}
	
	@Test
	public void testSwapDown() {
		GameManager gm = new GameManager();
		Game.cells[Game.COLUMNS - 1][Game.ROWS - 1] = new Cell(false);
		Game.cells[0][0].setValue(2);
		Game.cells[0][1].setValue(2);
		gm.game.swapDown();
		assertEquals(4, Game.cells[0][3].getValue());

	}
	
	@Test
	public void testSwapLeft() {
		GameManager gm = new GameManager();
		Game.cells[Game.COLUMNS - 1][Game.ROWS - 1] = new Cell(false);
		Game.cells[0][0].setValue(2);
		Game.cells[0][1].setValue(2);
		gm.game.swapLeft();
		assertEquals(4, Game.cells[0][3].getValue());

	}
	
	@Test
	public void testSwapRight() {
		GameManager gm = new GameManager();
		Game.cells[Game.COLUMNS - 1][Game.ROWS - 1] = new Cell(false);
		Game.cells[0][0].setValue(2);
		Game.cells[0][1].setValue(2);
		gm.game.swapRight();
		assertEquals(4, Game.cells[0][3].getValue());

	}
	
	@Test
	public void testScoreManager() {
		int expected = 48;
		GameManager gm = new GameManager();
		gm.scoreManager.updateScore(expected);
		assertEquals(expected+"", gm.scoreManager.getScore());

	}
	


}
