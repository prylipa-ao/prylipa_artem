package ua.khpi.prylipa05;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;

import org.junit.jupiter.api.Test;

import ua.khpi.prylipa03.View;
import ua.khpi.prylipa03.ViewResult;

// TODO: Auto-generated Javadoc
/**
 * The Class MainTest.
 */
class MainTest {

	/**
	 * Test restore comand.
	 */
	@Test
	void testRestoreComand() {
		View actual = new ViewResult();
		actual.viewInit();
		try {
			actual.viewSave();
		} catch (IOException e) {
			e.printStackTrace();
		}
		View expected = actual;
		RestoreCommand rc = new RestoreCommand(actual);
		rc.execute();
		assertEquals(expected, actual);
		assertEquals(4, rc.getKey());

	}

	/**
	 * Test init comand.
	 */
	@Test
	void testInitComand() {
		View actual = new ViewResult();
		View expected = new ViewResult();

		InitCommand ic = new InitCommand(actual);
		ic.execute();
		assertNotEquals(expected, actual);
		assertEquals(1, ic.getKey());
	}

	/**
	 * Test sort comand.
	 */
	@Test
	void testSortComand() {
		View actual = new ViewResult();
		View expected = actual;
		expected.viewSort();
		SortCommand sc = new SortCommand(actual);
		sc.execute();
		assertEquals(expected, actual);
		assertEquals(5, sc.getKey());
	}

	/**
	 * Test undo comand.
	 */
	@Test
	void testUndoComand() {
		View actual = new ViewResult();
		View expected = actual;
		expected.viewUndo();
		UndoCommand uc = new UndoCommand(actual);
		uc.execute();
		assertEquals(expected, actual);
		assertEquals(6, uc.getKey());
	}

}
