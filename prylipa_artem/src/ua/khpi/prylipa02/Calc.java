package ua.khpi.prylipa02;

import java.io.IOException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Arrays;


/**
 * The Class Calc implemented to calculate the task of an individual task.
 */
public class Calc implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The Constant FILENAME. */
	private static final String FILENAME = "Data.bin";

	/** The input data. */
	private Data data;

	/** The result. */
	private int result;

	/** The binary result. */
	private String binaryResult;

	/**
	 * Constructor.
	 */
	public Calc() {
		data = new Data();
		result = 0;
	}

	/**
	 * Sets the result.
	 *
	 * @param result
	 *            the new result
	 */
	public void setResult(int result) {
		this.result = result;
	}

	/**
	 * Gets the info about data - weight.
	 *
	 * @return the arg weight
	 */
	public double getArgWeight() {
		return data.getWeight();
	}

	/**
	 * Gets the info about data - height.
	 *
	 * @return the arg height
	 */
	public double getArgHeight() {
		return data.getHeight();
	}

	/**
	 * Gets the result.
	 *
	 * @return the result
	 */
	public int getResult() {
		return result;
	}

	/**
	 * Gets the binary result.
	 *
	 * @return the binary result
	 */
	public String getBinaryResult() {
		return binaryResult;
	}

	/**
	 * Inits the input data.
	 *
	 * @param weight
	 *            the weight
	 * @param height
	 *            the height
	 */
	public void init(double weight, double height) {
		data.setHeight(height);
		data.setWeight(weight);
	}

	/**
	 * Calculate the largest value of consecutively 1 in binary representation of
	 * the integer value of potential energy.
	 *
	 * @return the largest value of consecutively 1
	 */
	public int calc() {
		int potencialEnery = (int) (data.getHeight() * data.getWeight() * Data.accelerationOfGravity);
		binaryResult = Integer.toString(potencialEnery, 2);
		int[] tmp = new int[binaryResult.length()];
		int indx = 0;

		for (int i = 0; i < binaryResult.length() - 1; i++) {
			if (binaryResult.charAt(i) == '1') {
				tmp[indx]++;
			} else {
				indx++;
			}

		}
		Arrays.sort(tmp);
		result = tmp[binaryResult.length()-1];
		return result;
	}

	/**
	 * Show input data and result.
	 */
	public void show() {
		System.out.println("Param: " + data.getWeight() + " " + data.getHeight() + "\n" + binaryResult + " " + result);
	}

	/**
	 * Save object.
	 *
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void save() throws IOException {
		ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(FILENAME));
		os.writeObject(data);
		os.writeObject(result);
		os.flush();
		os.close();
	}

	/**
	 * Restore object.
	 *
	 * @throws Exception
	 *             the exception
	 */
	public void restore() throws Exception {
		ObjectInputStream is = new ObjectInputStream(new FileInputStream(FILENAME));
		data = (Data) is.readObject();
		result = (int) is.readObject();
		is.close();
	}

	/* 
	 * �onvert object to string
	 */
	@Override
	public String toString() {
		return "E = " + this.data.getWeight() + " * G * " + this.data.getHeight() + " = " + this.binaryResult + "->"
				+ this.result;
	}
}
