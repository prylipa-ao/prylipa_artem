package ua.khpi.prylipa02;

import java.io.Serializable;


/**
 * The Class Data stores raw data for calculating potential energy.
 */
public class Data implements Serializable {

	/** The Constant acceleration of gravity. */
	public static final float accelerationOfGravity = (float) 9.80665;
	
	/** The weight. */
	private double weight;
	
	/** The height. */
	private double height;
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	
	/**
	 * Constructor.
	 */
	public Data() {
		weight = 0;
		height = 0;
	}

	
	/**
	 * Instantiates a new data.
	 *
	 * @param weight the weight
	 * @param height the height
	 */
	public Data(double weight, double height) {
		this.weight = weight;
		this.height = height;
	}

	
	/**
	 * Sets the weight.
	 *
	 * @param weight the weight
	 * @return the double
	 */
	public double setWeight(double weight) {
		return this.weight = weight;
	}

	
	/**
	 * Gets the weight.
	 *
	 * @return the weight
	 */
	public double getWeight() {
		return weight;
	}

	
	/**
	 * Sets the height.
	 *
	 * @param height the height
	 * @return the double
	 */
	public double setHeight(double height) {
		return this.height = height;
	}


	/**
	 * Gets the height.
	 *
	 * @return the height
	 */
	public double getHeight() {
		return height;
	}

	
}