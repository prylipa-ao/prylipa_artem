package ua.khpi.prylipa02;

import java.io.IOException;

import java.util.Scanner;


/**
 * The Class Main.
 */
public class Main {

	/** The calc. */
	private Calc calc = new Calc();
	
	
	/**
	 * Menu.
	 */
	private void menu() {
		
		int choise;
		@SuppressWarnings("resource")
		Scanner in = new Scanner(System.in);
		do {

			
			System.out.print("Random value's(1) Calculate(2) Save(3) Restore(4) Exit\n");
			System.out.println("Choise command: ");
			choise = in.nextInt();

			switch (choise) {
			
			case 1:
				System.out.println("Generate new random values");
				calc.init((Math.random() * 50 + 1), (Math.random() * 50 + 1));
				break;
			case 2:
				System.out.println("Calculate done");
				calc.calc();
				calc.show();
				break;	
			case 3:
				System.out.println("Save done.");
				try {
					calc.save();
				} catch (IOException e) {
					System.out.println("Serialization error: " + e);
				}
				calc.show();
				break;
			case 4:
				System.out.println("Restore done.");
				try {
					calc.restore();
				} catch (Exception e) {
					System.out.println("Serialization error: " + e);
				}
				calc.show();
				break;
			case 5:
				System.out.println("Exit.");
				break;
			default:
				System.out.print("Enter comand 1-5\n");
			}
		} while (choise != 5);
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		Main main = new Main();
		main.menu();

	}
}
