package ua.khpi.prylipa03;

import java.io.IOException;
import java.util.Scanner;

/**
 * The Class Main.
 */
public class Main {

	/** The view. */
	private View view;

	/**
	 * Instantiates a new main.
	 *
	 * @param view
	 *            the view
	 */
	public Main(View view) {
		this.view = view;
	}

	/**
	 * Menu.
	 */
	protected void menu() {

		int choise;
		@SuppressWarnings("resource")
		Scanner in = new Scanner(System.in);
		do {

			System.out.print("Random value's(1) Show(2) Save(3) Restore(4) Exit\n");
			System.out.println("Choise command: ");
			choise = in.nextInt();

			switch (choise) {

			case 1:
				System.out.println("Generate new random values and calculate");
				view.viewInit();
				break;
			case 2:
				System.out.println("Calculate done");
				view.viewShow();
				break;
			case 3:
				System.out.println("Save done.");
				try {
					view.viewSave();
				} catch (IOException e) {
					System.out.println("Serialization error: " + e);
				}
				view.viewShow();
				break;
			case 4:
				System.out.println("Restore done.");
				try {
					view.viewRestore();
				} catch (Exception e) {
					System.out.println("Serialization error: " + e);
				}
				view.viewShow();
				break;
			case 5:
				System.out.println("Exit.");
				break;
			default:
				System.out.print("Enter comand 1-5\n");
			}
		} while (choise != 5);
	}

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		Main main = new Main(new ViewableResult().getView());
		main.menu();

	}
}
