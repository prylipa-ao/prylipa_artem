package ua.khpi.prylipa03;

/**
 * The Interface Viewable.
 */
public interface Viewable {

	/**
	 * Gets the view.
	 *
	 * @return the view
	 */
	public View getView();
}