package ua.khpi.prylipa03;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import ua.khpi.prylipa02.Calc;

/**
 * The Class ViewResult calcs and show result.
 */
public class ViewResult implements View {

	/** The Constant FILENAME. */
	private static final String FILENAME = "Data.bin";

	/** The count view. */
	private final int countView = 5;

	/** The list of source data */
	private ArrayList<Calc> calcs = new ArrayList<Calc>();

	/**
	 * Instantiates a new view result.
	 */
	public ViewResult() {
		for (int i = 0; i < countView; i++) {
			calcs.add(new Calc());
		}
	}

	/**
	 * Gets the items.
	 *
	 * @return the items
	 */
	public ArrayList<Calc> getItems() {
		return calcs;
	}

	/**
	 * Inits the list of source data.
	 */
	public void init() {
		for (Calc calc : calcs) {
			calc.init((Math.random() * 50 + 1), (Math.random() * 50 + 1));
			calc.calc();
		}
	}

	/**
	 * Inits the list of source data.
	 *
	 * @param weight
	 *            the weight
	 * @param height
	 *            the height
	 */
	public void init(double weight, double height) {
		for (Calc calc : calcs) {
			calc.init(weight, height);
			calc.calc();
		}
	}

	/**
	 * Init objects of random values.
	 */
	@Override
	public void viewInit() {
		this.init();
	}

	/**
	 * Save object to file.
	 *
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void viewSave() throws IOException {
		ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(FILENAME));
		os.writeObject(calcs);
		os.flush();
		os.close();
	}

	/**
	 * Restore object from file.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void viewRestore() throws Exception {
		ObjectInputStream is = new ObjectInputStream(new FileInputStream(FILENAME));
		calcs = (ArrayList<Calc>) is.readObject();
		is.close();
	}

	/**
	 * View header.
	 */
	@Override
	public void viewHeader() {
		System.out.println("Results:");
	}

	/**
	 * View body.
	 */
	@Override
	public void viewBody() {
		for (Calc calc : calcs) {
			System.out.println("Param: " + calc.getArgWeight() + " " + calc.getArgHeight() + "\nResult: "
					+ calc.getBinaryResult() + " -> " + calc.getResult());
		}
		System.out.println();
	}

	/**
	 * View footer.
	 */
	@Override
	public void viewFooter() {
		System.out.println("Calc done");
	}

	/**
	 * Show info about objects.
	 */
	@Override
	public void viewShow() {
		viewHeader();
		viewBody();
		viewFooter();
	}

	/**
	 * Sort objects by result.
	 */
	@Override
	public void viewSort() {
		try {
			viewSave();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Collections.sort(calcs, new Comparator<Calc>() {
			public int compare(Calc o1, Calc o2) {
				return (o1.getResult() + "").compareTo((o2.getResult() + ""));
			}
		});
	}

	/**
	 * Undo method. Cancels the command.
	 */
	@Override
	public void viewUndo() {
		try {
			viewRestore();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}