package ua.khpi.prylipa03;

import java.io.IOException;


/**
 * The Interface View for demonstrate Factory Method.
 */
public interface View {

	/**
	 * View header.
	 */
	public void viewHeader();

	/**
	 * View body.
	 */
	public void viewBody();

	/**
	 * View footer.
	 */
	public void viewFooter();

	/**
	 * Show info about objects.
	 */
	public void viewShow();

	/**
	 * Init objects of random values.
	 */
	public void viewInit();
	
	/**
	 * Save object to file.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void viewSave() throws IOException;

	/**
	 * Restore object from file.
	 *
	 * @throws Exception the exception
	 */
	public void viewRestore() throws Exception;
	/**
	 * Sort method for implementing factory pattern.
	 */
	public void viewSort();

	/**
	 * Undo method for implementing factory pattern. Cancels the command.
	 */
	public void viewUndo();
}