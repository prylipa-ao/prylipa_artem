package ua.khpi.prylipa03;

/**
 * The Class ViewableResult - design pattern (Factory Method)
 */
public class ViewableResult implements Viewable {

	/*
	 * Creates a display object
	 */
	@Override
	public View getView() {
		return new ViewResult();
	}
}
