package ua.khpi.prylipacp;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 * GameManager - managed process of game.
 */
public class GameManager {

	/** The game. */
	Game game;

	/** The game pane. */
	static TilePane gamePane;

	/** The backgroung pane. */
	final TilePane backgroungPane;

	/** The score manager. */
	static ScoreManager scoreManager = new ScoreManager();

	/** The current score. */
	static int currentScore;

	/**
	 * Instantiates a new game manager.
	 */
	GameManager() {
		game = new Game();
		gamePane = new TilePane();
		gamePane.setPrefColumns(Game.COLUMNS);
		gamePane.setPrefRows(Game.ROWS);

		backgroungPane = new TilePane();
		backgroungPane.setPrefColumns(Game.COLUMNS);
		backgroungPane.setPrefRows(Game.ROWS);

		for (int r = 0; r < Game.ROWS; r++) {
			for (int c = 0; c < Game.COLUMNS; c++) {
				Game.cells[r][c] = new Cell(false);
				gamePane.getChildren().add(Game.cells[r][c]);
				Game.cells[r][c].refresh();
				backgroungPane.getChildren().add(new Cell(true));
			}
		}
		newGame();
	}

	/**
	 * Builds the scene and add key event handlers.
	 *
	 * @return the scene
	 */
	Scene buildScene() {
		final Scene scene = new Scene(buildContent());
		scene.addEventFilter(KeyEvent.KEY_PRESSED, event -> {

			if (game.isOver()) {
				showGameOverDialog();
				newGame();
			}
			switch (event.getCode()) {
			case UP:
				game.swapUp();
				break;
			case DOWN:
				game.swapDown();
				break;
			case LEFT:
				game.swapLeft();
				break;
			case RIGHT:
				game.swapRight();
				break;
			default:
				break;
			}

		});
		return scene;
	}

	/**
	 * Start new game.
	 */
	static void newGame() {
		for (int r = 0; r < Game.ROWS; r++) {
			for (int c = 0; c < Game.COLUMNS; c++) {
				Game.cells[r][c].setValue(0);
				Game.cells[r][c].refresh();
			}
		}
		for (int count = 0; count < (int) (Math.random() * 3) + 1; count++) {
			Game.generateNewValue();
		}
		GameManager.currentScore = 0;
		GameManager.scoreManager.updateScore(GameManager.currentScore);
	}

	/**
	 * Show game over dialog.
	 */
	void showGameOverDialog() {
		final Alert alert = new Alert(Alert.AlertType.INFORMATION);
		alert.getDialogPane().setHeaderText("GAME OVER");
		alert.setContentText("Your score: " + currentScore);
		alert.show();

	}

	/**
	 * Builds view parts of application.
	 *
	 * @return view elements of application
	 */
	Parent buildContent() {
		final Node newGame = createButtonNewGameView();
		final Node grid = createMainView();

		final VBox root = new VBox();
		root.setSpacing(20);
		root.getChildren().addAll(scoreManager.getScoreView(), newGame, grid);

		root.setPadding(new Insets(55));
		return root;
	}

	/**
	 * Creates the main view.
	 *
	 * @return view of the application game field
	 */
	private Node createMainView() {
		final StackPane stackPane = new StackPane();
		stackPane.getChildren().addAll(backgroungPane, gamePane);

		final StackPane parent = new StackPane();

		final BackgroundFill fill = new BackgroundFill(InterfaceUtil.getGridBgColor(), new CornerRadii(10), null);
		final Background background = new Background(fill);
		parent.setBackground(background);

		parent.setPadding(new Insets(8));
		parent.getChildren().add(stackPane);

		return parent;
	}

	/**
	 * Creates the button new game view.
	 *
	 * @return view of the button "New Game"
	 */
	private Node createButtonNewGameView() {
		final BackgroundFill background = new BackgroundFill(InterfaceUtil.getNewGameBgColor(), new CornerRadii(10),
				null);
		final StackPane newGame = new StackPane();
		final Label label = new Label("New Game");
		label.setFont(Font.font(16));
		label.setTextFill(Color.WHITE);
		newGame.setBackground(new Background(background));
		newGame.setPadding(new Insets(8));
		newGame.getChildren().add(label);
		newGame.setOnMouseClicked(event -> newGame());
		final HBox hBox = new HBox(newGame);
		hBox.setAlignment(Pos.CENTER);
		return hBox;
	}

}
