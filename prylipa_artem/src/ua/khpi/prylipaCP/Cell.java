package ua.khpi.prylipacp;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;

/**
 * Cell - implement game cell
 */
public class Cell extends StackPane {

	/** constant width cell. */
	static final int WIDTH_CELL = 85;

	/** constant height cell. */
	static final int HEIGHT_CELL = 85;

	/** The view value of cell. */
	private final Label viewValue = new Label();

	/** The model of cell. */
	private final Rectangle modelOfCell;

	/** The flag of opportunities is merged. */
	boolean isMerged;

	/** The value of cell. */
	private int value;

	/**
	 * Instantiates a new cell.
	 *
	 * @param isBackground
	 *            the flag of fade background cell
	 */
	public Cell(boolean isBackground) {
		setPadding(new Insets(WIDTH_CELL / 16));
		setAlignment(Pos.CENTER);

		modelOfCell = new Rectangle(WIDTH_CELL, HEIGHT_CELL);
		modelOfCell.setArcWidth(25);
		modelOfCell.setArcHeight(25);
		if (isBackground) {
			modelOfCell.setFill(InterfaceUtil.getEmptyColor());
		}
		getChildren().add(modelOfCell);

		viewValue.setFont(Font.font(35));

		final DropShadow shadow = new DropShadow();
		shadow.setColor(Color.BLACK);
		shadow.setRadius(1.8);
		shadow.setOffsetX(1.5);
		shadow.setOffsetY(1);
		viewValue.setEffect(shadow);
		viewValue.setTextFill(Color.WHITE);

		getChildren().add(viewValue);
	}

	/**
	 * Update view of cell.
	 */
	public void refresh() {
		if (this.value == 0) {
			setOpacity(0);
		} else {
			setOpacity(1);
			viewValue.setText(this.toString());
		}
		if (isMerged) {
			InterfaceUtil.mergeAnimation(this).play();
		}
		isMerged = false;

		modelOfCell.setFill(InterfaceUtil.getColor(this.value));
	}

	/**
	 * Gets the value of cell.
	 *
	 * @return the value of cell
	 */
	public int getValue() {
		return value;
	}

	/**
	 * Sets the value of cell.
	 *
	 * @param num
	 *            the new value of cell
	 */
	public void setValue(int num) {
		this.value = num;
	}

	/*
	 * Convert value of cell to string
	 * 
	 * @see javafx.scene.Node#toString()
	 */
	@Override
	public String toString() {
		return this.value + "";
	}
}
