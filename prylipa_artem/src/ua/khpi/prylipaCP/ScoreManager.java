package ua.khpi.prylipacp;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 * ScoreManager - managed score of game.
 */
public class ScoreManager {

	/** The score pane. */
	private final StackPane scorePane;

	/** The score value. */
	private final Label scoreValue;

	/**
	 * Instantiates a new score manager.
	 */
	public ScoreManager() {
		final Background background = new Background(
				new BackgroundFill(InterfaceUtil.getScoreBgColor(), new CornerRadii(30), null));
		final Label label = new Label("YOUR SCORE");
		label.setFont(Font.font(16));
		label.setTextFill(Color.WHITE);

		final VBox scoreContainer = new VBox();
		scoreContainer.setAlignment(Pos.CENTER);
		scoreContainer.setBackground(background);
		scoreValue = new Label("0");
		scoreValue.setFont(Font.font(20));
		scoreValue.setTextFill(Color.WHITE);

		scoreContainer.getChildren().addAll(label, scoreValue);
		scorePane = new StackPane(scoreContainer);
	}

	/**
	 * Update score.
	 *
	 * @param score
	 *            the new value of score
	 */
	public void updateScore(int score) {
		scoreValue.setText(String.valueOf(score));
	}

	/**
	 * Gets the value of score.
	 *
	 * @return the value of score
	 */
	public String getScore() {
		return scoreValue.getText();
	}

	/**
	 * Gets the score view.
	 *
	 * @return the score view
	 */
	public Node getScoreView() {
		return scorePane;
	}

}
