package ua.khpi.prylipacp;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Main - run application.
 */
public class Main extends Application {

	/**
	 * The entry point to program.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		launch(args);
	}

	/*
	 * Start JavaFx Application
	 * 
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage stage) {
		stage.setTitle("2048 Game");
		stage.setScene(new GameManager().buildScene());
		stage.setResizable(false);
		stage.show();
	}
}
