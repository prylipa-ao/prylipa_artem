package ua.khpi.prylipacp;

import javafx.animation.ParallelTransition;

import java.util.Random;

/**
 * Game - implement function of 2048 Game
 */
public class Game {

	/** The count rows of game field. */
	static final int ROWS = 4;

	/** The count columns of game field. */
	static final int COLUMNS = 4;

	/** The cells. */
	static Cell[][] cells = new Cell[ROWS][COLUMNS];

	/**
	 * Update game field.
	 */
	private void reload() {
		GameManager.gamePane.getChildren().clear();
		for (int r = 0; r < 4; r++) {
			for (int c = 0; c < 4; c++) {
				final Cell cell = cells[r][c];
				GameManager.gamePane.getChildren().add(cell);
				cell.refresh();
			}
		}
	}

	/**
	 * Checks game is over.
	 *
	 * @return true, if game is over
	 */
	boolean isOver() {
		if (!isFull()) {
			return false;
		}

		for (int r = 0; r < ROWS; r++) {
			Cell left = cells[r][0];
			for (int c = 1; c < COLUMNS; c++) {
				final Cell current = cells[r][c];
				if (current.getValue() == left.getValue()) {
					return false;
				}
				left = current;
			}
		}

		for (int c = 0; c < COLUMNS; c++) {
			Cell above = cells[0][c];
			for (int r = 1; r < ROWS; r++) {
				final Cell current = cells[r][c];
				if (current.getValue() == above.getValue()) {
					return false;
				}
				above = current;
			}
		}

		return true;
	}

	/**
	 * Checks if game field is full.
	 *
	 * @return true, if game field is full
	 */
	private static boolean isFull() {
		for (int r = 0; r < ROWS; r++) {
			for (int c = 0; c < COLUMNS; c++) {
				if (cells[r][c].getValue() == 0) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Generate new value of cell.
	 */
	static void generateNewValue() {
		if (!isFull()) {

			final Random random = new Random(System.currentTimeMillis());
			boolean done = false;
			int r = random.nextInt(ROWS);
			int c = random.nextInt(COLUMNS);
			while (!done) {
				final Cell cell = cells[r][c];
				if (cell.getValue() == 0) {
					if (Math.random() > 0.75) {
						cell.setValue(4);
						done = true;
					} else {
						cell.setValue(2);
						done = true;
					}
				} else {
					r = random.nextInt(ROWS);
					c = random.nextInt(COLUMNS);
				}
				cell.refresh();

			}

		}
	}

	/**
	 * Swap cell from start position to finish position.
	 *
	 * @param r0
	 *            the start position of row
	 * @param c0
	 *            the start position of column
	 * @param r1
	 *            the finish position of row
	 * @param c1
	 *            the finish position of column
	 */
	private void swap(int r0, int c0, int r1, int c1) {
		final Cell tmp = cells[r0][c0];
		cells[r0][c0] = cells[r1][c1];
		cells[r1][c1] = tmp;
	}

	/**
	 * Swap cells left.
	 */
	void swapLeft() {
		final ParallelTransition pt = new ParallelTransition();
		for (int r = 0; r < ROWS; r++) {
			for (int c = 1; c < COLUMNS; c++) {
				final Cell key = cells[r][c];
				if (key.getValue() == 0) {
					continue;
				}
				int step = 0;
				for (int tc = c; tc > 0; tc--) {
					final Cell current = cells[r][tc];
					final Cell left = cells[r][tc - 1];
					if (left.getValue() == 0) {
						swap(r, tc - 1, r, tc);
						step++;
					} else if (tryMerging(left, current)) {
						step++;
					}
				}
				pt.getChildren().add(InterfaceUtil.swapAnimation(key, -Cell.WIDTH_CELL * step, 0));
			}
		}
		pt.play();
		pt.setOnFinished(event -> {
			reload();
			generateNewValue();
		});
	}

	/**
	 * Swap cells right.
	 */
	void swapRight() {
		final ParallelTransition pt = new ParallelTransition();
		for (int r = 0; r < ROWS; r++) {
			for (int c = COLUMNS - 2; c >= 0; c--) {
				final Cell key = cells[r][c];
				if (key.getValue() == 0) {
					continue;
				}
				int step = 0;
				for (int tc = c; tc < COLUMNS - 1; tc++) {
					final Cell current = cells[r][tc];
					final Cell right = cells[r][tc + 1];
					if (right.getValue() == 0) {
						swap(r, tc, r, tc + 1);
						step++;
					} else if (tryMerging(right, current)) {
						step++;
					}
				}
				pt.getChildren().add(InterfaceUtil.swapAnimation(key, Cell.WIDTH_CELL * step, 0));
			}
		}
		pt.play();
		pt.setOnFinished(event -> {
			reload();
			generateNewValue();
		});
	}

	/**
	 * Swap cells up.
	 */
	void swapUp() {
		final ParallelTransition pt = new ParallelTransition();
		for (int c = 0; c < COLUMNS; c++) {
			for (int r = 1; r < ROWS; r++) {
				final Cell key = cells[r][c];
				if (key.getValue() == 0) {
					continue;
				}

				int step = 0;
				for (int tr = r; tr > 0; tr--) {
					final Cell current = cells[tr][c];
					final Cell above = cells[tr - 1][c];
					if (above.getValue() == 0) {
						swap(tr - 1, c, tr, c);
						step++;
					} else if (tryMerging(above, current)) {
						step++;
					}
					pt.getChildren().addAll(InterfaceUtil.swapAnimation(key, 0, -Cell.HEIGHT_CELL * step));
				}
			}
		}
		pt.play();
		pt.setOnFinished(event -> {
			reload();
			generateNewValue();
		});
	}

	/**
	 * Swap cells down.
	 */
	void swapDown() {
		final ParallelTransition pt = new ParallelTransition();
		for (int c = 0; c < COLUMNS; c++) {
			for (int r = 2; r >= 0; r--) {
				final Cell key = cells[r][c];
				if (key.getValue() == 0) {
					continue;
				}
				int step = 0;
				for (int tr = r; tr < ROWS - 1; tr++) {
					final Cell current = cells[tr][c];
					final Cell below = cells[tr + 1][c];
					if (below.getValue() == 0) {
						swap(tr, c, tr + 1, c);
						step++;
					} else if (tryMerging(below, current)) {
						step++;
					}
				}
				pt.getChildren().addAll(InterfaceUtil.swapAnimation(key, 0, Cell.HEIGHT_CELL * step));
			}
		}
		pt.play();
		pt.setOnFinished(event -> {
			reload();
			generateNewValue();
		});
	}

	/**
	 * Check try merging.
	 *
	 * @param target
	 *            the target cell
	 * @param source
	 *            the source cell
	 * @return true, if successful
	 */
	private boolean tryMerging(Cell target, Cell source) {
		if (target.getValue() != source.getValue()) {
			return false;
		}
		if (target.isMerged || source.isMerged) {
			return false;
		}
		final int num = target.getValue() << 1;
		target.setValue(num);
		source.setValue(0);
		GameManager.currentScore += num;
		GameManager.scoreManager.updateScore(GameManager.currentScore);
		target.isMerged = true;
		source.isMerged = true;
		return true;
	}

}
