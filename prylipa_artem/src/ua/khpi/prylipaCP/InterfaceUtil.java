package ua.khpi.prylipacp;

import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.util.Duration;

import java.util.HashMap;
import java.util.Map;

/**
 * InterfaceUtil - templates transaction, animation of swap and contains colors
 * of interface.
 */
public class InterfaceUtil {

	/** The collors of cells. */
	private static Map<Integer, Color> collorsOfCells = new HashMap<>();

	/** Color of empty cell. */
	final private static Color colorOfEmptyCell = Color.web("rgba(238, 228, 218, 0.35)");

	/** Background Color of game field. */
	final private static Color mainBackgroundColor = Color.DARKCYAN;

	/** Button "newGame" backgroung color. */
	final private static Color newGameBackgroundColor = Color.DARKCYAN;

	/** View score backgound color. */
	final private static Color scoreBackgoundColor = Color.DARKCYAN;

	static {
		collorsOfCells.put(2, Color.POWDERBLUE);
		collorsOfCells.put(4, Color.AQUAMARINE);
		collorsOfCells.put(8, Color.LIGHTSKYBLUE);
		collorsOfCells.put(16, Color.DODGERBLUE);
		collorsOfCells.put(32, Color.LIGHTGREEN);
		collorsOfCells.put(64, Color.LIGHTSEAGREEN);
		collorsOfCells.put(128, Color.AQUA);
		collorsOfCells.put(256, Color.GREENYELLOW);
		collorsOfCells.put(512, Color.DARKSLATEGREY);
		collorsOfCells.put(1024, Color.SLATEGREY);
		collorsOfCells.put(2048, Color.BLACK);
		collorsOfCells.put(4096, Color.CORAL);
		collorsOfCells.put(8192, Color.CHOCOLATE);
	}

	/**
	 * Merge animation.
	 *
	 * @param node
	 *            the node
	 * @return the scale transition
	 */
	static ScaleTransition mergeAnimation(Node node) {
		final ScaleTransition scale = new ScaleTransition(Duration.millis(200), node);
		scale.setFromX(0.618);
		scale.setFromY(0.618);
		scale.setToX(1.2);
		scale.setToY(1.2);
		scale.setOnFinished(event -> {
			node.setScaleX(1);
			node.setScaleY(1);
		});
		return scale;
	}

	/**
	 * Swap animation.
	 *
	 * @param node
	 *            the node
	 * @param x
	 *            the x
	 * @param y
	 *            the y
	 * @return the translate transition
	 */
	static TranslateTransition swapAnimation(Node node, int x, int y) {
		final TranslateTransition tr = new TranslateTransition(Duration.millis(100), node);
		tr.setByX(x);
		tr.setByY(y);
		tr.setOnFinished(event -> {
			node.setTranslateX(0);
			node.setTranslateY(0);
		});
		return tr;
	}

	/**
	 * Gets the color of cells.
	 *
	 * @param num
	 *            of the cell
	 * @return the color
	 */
	static Color getColor(int num) {
		if (collorsOfCells.containsKey(num)) {
			return collorsOfCells.get(num);
		}
		return Color.GRAY;
	}

	/**
	 * Gets color of the empty cell.
	 *
	 * @return the empty color
	 */
	static Color getEmptyColor() {
		return colorOfEmptyCell;
	}

	/**
	 * Gets color of the grid background.
	 *
	 * @return the grid background color
	 */
	static Color getGridBgColor() {
		return mainBackgroundColor;
	}

	/**
	 * Gets color the button "new game".
	 *
	 * @return the new game button color
	 */
	static Color getNewGameBgColor() {
		return newGameBackgroundColor;
	}

	/**
	 * Gets color of the score panel.
	 *
	 * @return the score panel color
	 */
	static Color getScoreBgColor() {
		return scoreBackgoundColor;
	}

}
