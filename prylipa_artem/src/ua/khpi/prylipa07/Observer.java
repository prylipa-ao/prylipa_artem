package ua.khpi.prylipa07;

/**
 * The Interface Observer binds the observer and method.
 */
public interface Observer {

	/**
	 * Handler of event.
	 *
	 * @param observable
	 *            the observable of method
	 * @param event
	 *            the info about event
	 */
	public void handleEvent(Observable observable, Object event);
}
