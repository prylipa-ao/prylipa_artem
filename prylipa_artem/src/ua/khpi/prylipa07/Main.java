package ua.khpi.prylipa07;

import ua.khpi.prylipa05.ConsoleCommand;
import ua.khpi.prylipa05.Menu;

/**
 * The Class Main.
 */
public class Main {

	/**
	 * The Class ConsoleCmd.
	 */
	abstract class ConsoleCmd implements ConsoleCommand {

		/** The collection. */
		protected Collection items;

		/** The key of command. */
		private int key;

		/**
		 * Instantiates a new console cmd.
		 *
		 * @param items
		 *            the items
		 * @param key
		 *            the key
		 */
		ConsoleCmd(Collection items, int key) {
			this.items = items;
			this.key = key;
		}

		/*
		 * Get key of command
		 * 
		 * @see ua.khpi.prylipa05.ConsoleCommand#getKey()
		 */
		@Override
		public int getKey() {
			return key;
		}

	}

	/**
	 * Run.
	 */
	public void run() {
		Collection items = new Collection();
		InitEventHandler init = new InitEventHandler();
		EventHanlders eventHandlers = new EventHanlders();

		items.addObserver(init);
		items.addObserver(eventHandlers);
		Menu menu = new Menu(7);

		menu.add(new ConsoleCmd(items, 1) {
			@Override
			public void execute() {
				items.add(new Data(""));
			}
		});

		menu.add(new ConsoleCmd(items, 2) {
			@Override
			public void execute() {
				items.delete((int) Math.round(Math.random() * (items.getCollection().size() - 1)));
			}
		});
		menu.add(new ConsoleCmd(items, 3) {
			@Override
			public void execute() {
				items.searchMin();
			}
		});
		menu.execute();
		menu.add(new ConsoleCmd(items, 4) {
			@Override
			public void execute() {
				System.out.println(items.getCollection());
			}
		});
	}

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		new Main().run();
	}
}
