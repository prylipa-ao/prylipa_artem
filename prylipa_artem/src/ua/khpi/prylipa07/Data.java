package ua.khpi.prylipa07;

/**
 * The Data - info about object.
 */
public class Data implements Comparable<Data> {

	/** The data. */
	private String data;

	/**
	 * Instantiates a new data.
	 *
	 * @param data
	 *            the data
	 */
	public Data(String data) {
		this.setData(data);
	}

	/**
	 * Gets the data.
	 *
	 * @return the data
	 */
	public String getData() {
		return data;
	}

	/**
	 * Sets the data.
	 *
	 * @param data
	 *            the new data
	 */
	public void setData(String data) {
		this.data = data;
	}

	/*
	 * Convert object to string
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return data;
	}

	/*
	 * Compare objects
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Data o) {
		return data.compareTo(o.data);
	}
}
