package ua.khpi.prylipa07;

/**
 * InitEventHandler - generate value for object.
 */
public class InitEventHandler extends AnnotedObserver {

	/**
	 * Generate random value if object empty.
	 *
	 * @param collection
	 *            the collection
	 */
	@Event(Collection.ITEMS_EMPTY)
	public void collectionEmpty(Collection collection) {

		for (Data data : collection) {
			if (data.getData().isEmpty()) {
				int size = (int) (Math.random() * 10) + 1;
				String tmpData = "";
				for (int i = 1; i < size; i++) {
					tmpData += (char) ((int) (Math.random() * 26) + 'A');

				}
				data.setData(tmpData);
			}
		}
		collection.call(Collection.ITEMS_CHANGED);
	}
}
