package ua.khpi.prylipa07;

import java.util.Collections;
import java.util.Comparator;

/**
 * EventHanlders - treatmenting of specified events.
 */
public class EventHanlders extends AnnotedObserver {

	/** The event ID of modify. */
	public static final String ITEMS_MODIFY = "ITEMS_MODIFY";

	/**
	 * Event Handler(ITEMS_CHANGED) - Sort collection.
	 *
	 * @param collection
	 *            the collection
	 */
	@Event(Collection.ITEMS_CHANGED)
	public void collectionModify(Collection collection) {
		Collections.sort(collection.getCollection());
		collection.call(ITEMS_MODIFY);
	}

	/**
	 * Event Handler(ITEM_SEARCHMIN) - Show object thiw min length.
	 *
	 * @param collection
	 *            the collection
	 */
	@Event(Collection.ITEM_SEARCHMIN)
	public void showMinLen(Collection collection) {
		Collections.sort(collection.getCollection(), new Comparator<Data>() {
			public int compare(Data o1, Data o2) {
				return (o1.toString().length() + "").compareTo((o2.toString().length() + ""));
			}
		});
		collection.call(ITEMS_MODIFY);
		System.out.println("Min length: " + collection.getCollection().get(0));
	}

	/**
	 * Event Handler(ITEMS_MODIFY) - Show collection.
	 *
	 * @param collection
	 *            the collection
	 */
	@Event(ITEMS_MODIFY)
	public void showResult(Collection collection) {
		System.out.println(collection.getCollection());
	}

	/**
	 * Event Handler(ITEMS_REMOVED) - Show collection before remove object.
	 *
	 * @param collection
	 *            the collection
	 */
	@Event(Collection.ITEMS_REMOVED)
	public void removed(Collection collection) {
		System.out.println(collection.getCollection());
	}

}
