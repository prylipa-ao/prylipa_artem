package ua.khpi.prylipa07;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Contains objects and identificators of event.
 */
public class Collection extends Observable implements Iterable<Data> {

	/** The event ID of changed object */
	public static final String ITEMS_CHANGED = "ITEMS_CHANGED";

	/** The event ID of empty object. */
	public static final String ITEMS_EMPTY = "ITEMS_EMPTY";

	/** The event Id of remove objects. */
	public static final String ITEMS_REMOVED = "ITEMS_REMOVED";

	/** The event ID of seach object with minimal length. */
	public static final String ITEM_SEARCHMIN = "ITEM_SEARCHMIN";

	/** The collection objects. */
	private List<Data> collection = new ArrayList<Data>();

	/**
	 * Add object to collection.
	 *
	 * @param item
	 *            the item
	 */
	public void add(Data item) {
		collection.add(item);
		if (item.getData().isEmpty())
			call(ITEMS_EMPTY);
		else
			call(ITEMS_CHANGED);
	}

	/**
	 * Delete object by index.
	 *
	 * @param index
	 *            the index of deleted object
	 */
	public void delete(int index) {
		collection.remove(index);
		call(ITEMS_REMOVED);
	}

	/**
	 * Delete object.
	 *
	 * @param data
	 *            the deleted object
	 */
	public void delete(Data data) {
		collection.remove(data);
		call(ITEMS_REMOVED);
	}

	/**
	 * Search object with min lenght.
	 */
	public void searchMin() {
		call(ITEM_SEARCHMIN);
	}

	/**
	 * Gets the collection.
	 *
	 * @return the collection
	 */
	public List<Data> getCollection() {
		return collection;
	}

	/*
	 * Iterator for collection
	 * 
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<Data> iterator() {
		return collection.iterator();
	}

}
