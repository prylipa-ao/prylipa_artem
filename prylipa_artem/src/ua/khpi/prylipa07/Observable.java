package ua.khpi.prylipa07;

import java.util.HashSet;
import java.util.Set;

/**
 * Observable - binding observers with methods.
 */
public abstract class Observable {

	/** The collection of observers. */
	private Set<Observer> observers = new HashSet<Observer>();

	/**
	 * Adds the observer.
	 *
	 * @param observer
	 *            the new observer
	 */
	public void addObserver(Observer observer) {
		observers.add(observer);
	}

	/**
	 * Delete observer.
	 *
	 * @param observer
	 *            the observer
	 */
	public void delObserver(Observer observer) {
		observers.remove(observer);
	}

	/**
	 * Call event.
	 *
	 * @param event
	 *            the event
	 */
	public void call(Object event) {
		for (Observer observer : observers) {
			observer.handleEvent(this, event);
		}
	}
}
