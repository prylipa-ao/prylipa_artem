package ua.khpi.prylipa07;

import java.lang.annotation.*;

/**
 * The annotation of event.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Event {
	String value();
}
