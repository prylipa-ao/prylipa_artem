package ua.khpi.prylipa07;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * An asynchronous update interface for receiving notifications about Annoted
 * information as the Annoted is constructed.
 */
public abstract class AnnotedObserver implements Observer {

	/** The handlers events. */
	private Map<Object, Method> handlers = new HashMap<Object, Method>();

	/**
	 * This method is called when information about an Annoted which was previously
	 * requested using an asynchronous interface becomes available.
	 */
	public AnnotedObserver() {
		for (Method method : this.getClass().getMethods()) {
			if (method.isAnnotationPresent(Event.class)) {
				handlers.put(method.getAnnotation(Event.class).value(), method);
			}

		}
	}

	/*
	 * Handler of event. �alls the handler for the selected method.
	 * 
	 * @see ua.khpi.prylipa07.Observer#handleEvent(ua.khpi.prylipa07.Observable,
	 * java.lang.Object)
	 */
	@Override
	public void handleEvent(Observable observable, Object event) {
		Method method = handlers.get(event);

		if (method != null)
			try {
				method.invoke(this, observable);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			}
	}
}
