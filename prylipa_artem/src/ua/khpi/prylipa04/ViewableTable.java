package ua.khpi.prylipa04;

import ua.khpi.prylipa03.*;

/**
 * The Class ViewableTable - design pattern (Factory Method).
 */
public class ViewableTable extends ViewableResult {

	/*
	 * Creates a display object
	 */
	@Override
	public View getView() {
		return new ViewTable();
	}
}
