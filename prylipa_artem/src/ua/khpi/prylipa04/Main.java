package ua.khpi.prylipa04;

import ua.khpi.prylipa03.View;

/**
 * The Class Main.
 */
public class Main extends ua.khpi.prylipa03.Main {

	/**
	 * Instantiates a new main.
	 *
	 * @param view the view
	 */
	public Main(View view) {
		super(view);
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		Main main = new Main(new ViewableTable().getView());
		main.menu();
	}
}