package ua.khpi.prylipa04;

import java.util.Formatter;

import ua.khpi.prylipa02.*;
import ua.khpi.prylipa03.ViewResult;

/**
 * The Class ViewTable - implementation of data output in the form of a table.
 */
public class ViewTable extends ViewResult {

	/** The Constant DEFAULT_WIDTH. */
	private static final int DEFAULT_WIDTH = 50;

	/** The width. */
	private int width;

	/**
	 * Instantiates a new view table.
	 */
	public ViewTable() {
		width = DEFAULT_WIDTH;
	}

	/**
	 * Sets the width.
	 *
	 * @param width
	 *            the width
	 */
	public int setWidth(int width) {
		return this.width = width;
	}

	/**
	 * Gets the width.
	 *
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * Draw line.
	 */
	private void printLine() {
		for (int i = 0; i < width; i++) {
			System.out.print('-');
		}
		System.out.println();
	}

	/**
	 * Out header.
	 */
	private void printHeader() {
		Formatter fmt = new Formatter();
		fmt.format(" %s| %s | %s | %s\n", "Weight ", "Height", "Result", "Binary result");
		System.out.print(fmt);

	}

	/**
	 * Out body.
	 */
	private void printBody() {
		Formatter fmt = new Formatter();

		for (Calc calc : getItems()) {
			fmt.format(" %d\t| %d\t | %d\t | %s\n", (int) calc.getArgWeight(), (int) calc.getArgHeight(),
					calc.getResult(), calc.getBinaryResult());
			System.out.print(fmt);
		}
	}

	/**
	 * Inits the.
	 *
	 * @param width
	 *            the width
	 */
	public final void init(int width) {
		this.width = width;
		init((Math.random() * 50 + 1), (Math.random() * 50 + 1));
	}

	/*
	 * Init objects of random values
	 * 
	 * @see ua.khpi.prylipa03.ViewResult#init()
	 */
	@Override
	public void init() {
		System.out.print("Initialization done ");
		super.init();
	}

	/*
	 * View header of table
	 * 
	 * @see ua.khpi.prylipa03.ViewResult#viewHeader()
	 */
	@Override
	public void viewHeader() {
		printHeader();
		printLine();
	}

	/*
	 * View body of table
	 * 
	 * @see ua.khpi.prylipa03.ViewResult#viewBody()
	 */
	@Override
	public void viewBody() {
		printBody();
	}

	/*
	 * View footer of table
	 * 
	 * @see ua.khpi.prylipa03.ViewResult#viewFooter()
	 */
	@Override
	public void viewFooter() {
		printLine();
	}
}