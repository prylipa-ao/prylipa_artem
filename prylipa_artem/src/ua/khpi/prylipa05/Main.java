package ua.khpi.prylipa05;

/**
 * The Class Main.
 */
public class Main {
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		Application app = Application.getInstance();
		app.run();
	}
}
