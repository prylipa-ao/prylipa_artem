package ua.khpi.prylipa05;

import ua.khpi.prylipa03.View;

/**
 * Console command - Init.
 */
public class InitCommand implements ConsoleCommand {
	
	/** The view. */
	private View view;

	/**
	 * Instantiates a new inits the command.
	 *
	 * @param view the view
	 */
	public InitCommand(View view) {
		this.view = view;
	}

	/* Run init command
	 * @see ua.khpi.prylipa05.Command#execute()
	 */
	@Override
	public void execute() {
		System.out.println("Generate new random values and calculate");
		view.viewInit();

	}

	/* Get key of command
	 * @see ua.khpi.prylipa05.ConsoleCommand#getKey()
	 */
	@Override
	public int getKey() {
		return 1;
	}

}
