package ua.khpi.prylipa05;

/**
 * The Interface of command.
 */
public interface Command {

	/**
	 * Execute command.
	 */
	public void execute();
}
