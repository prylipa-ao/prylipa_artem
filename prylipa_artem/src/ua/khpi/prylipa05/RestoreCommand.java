package ua.khpi.prylipa05;

import ua.khpi.prylipa03.View;

/**
 * Console command - Restore.
 */
public class RestoreCommand implements ConsoleCommand {
	
	/** The view. */
	private View view;

	/**
	 * Instantiates a new restore command.
	 *
	 * @param view the view
	 */
	public RestoreCommand(View view) {
		this.view = view;
	}

	/* Run restore command
	 * @see ua.khpi.prylipa05.Command#execute()
	 */
	@Override
	public void execute() {
		System.out.println("Restore done.");
		try {
			view.viewRestore();
		} catch (Exception e) {
			System.out.println("Serialization error: " + e);
		}
		view.viewShow();

	}

	/* Get key of command
	 * @see ua.khpi.prylipa05.ConsoleCommand#getKey()
	 */
	@Override
	public int getKey() {
		return 4;
	}

}
