package ua.khpi.prylipa05;

/**
 * The Interface of console command.
 */
public interface ConsoleCommand extends Command {
	
	/**
	 * Gets the key of command.
	 *
	 * @return the key
	 */
	public int getKey();
}
