package ua.khpi.prylipa05;

import ua.khpi.prylipa03.View;

/**
 * Console command - Undo.
 */
public class UndoCommand implements ConsoleCommand {
	
	/** The view. */
	private View view;

	/**
	 * Instantiates a new undo command.
	 *
	 * @param view the view
	 */
	public UndoCommand(View view) {
		this.view = view;
	}

	/* Run undo command
	 * @see ua.khpi.prylipa05.Command#execute()
	 */
	@Override
	public void execute() {
		System.out.println("Undo done");
		view.viewUndo();

	}

	/* Get key of command
	 * @see ua.khpi.prylipa05.ConsoleCommand#getKey()
	 */
	@Override
	public int getKey() {
		return 6;
	}

}
