package ua.khpi.prylipa05;

import ua.khpi.prylipa03.View;
import ua.khpi.prylipa03.ViewableResult;
import ua.khpi.prylipa06.RunThreadsCommand;

/**
 * Application - forms the menu and launches the application.
 */
public class Application {
	
	/** The instance. */
	private static Application instance = new Application();

	/** The view. */
	private View view; 

	/** The menu. */
	private Menu menu = new Menu(6);

	/**
	 * Instantiates a new application.
	 */
	private Application() {
		this.view = new ViewableResult().getView();
	}
	
	/**
	 * Instantiates a new application.
	 *
	 * @param winView the win view
	 */
	public Application(View winView) {
		this.view = winView;
	}
	
	/**
	 * Gets the single instance of Application.
	 *
	 * @return single instance of Application
	 */
	public static Application getInstance() {
		return instance;
	}

	/**
	 * Run application.
	 */
	public void run() {
		menu.add(new ShowCommand(view));
		menu.add(new InitCommand(view));
		menu.add(new SaveCommand(view));
		menu.add(new RestoreCommand(view));
		menu.add(new SortCommand(view));
		menu.add(new UndoCommand(view));
		menu.add(new RunThreadsCommand(view));
		menu.execute();
	}

	
}
