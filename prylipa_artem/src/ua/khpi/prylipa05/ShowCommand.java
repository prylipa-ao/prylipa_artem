package ua.khpi.prylipa05;

import ua.khpi.prylipa03.View;

/**
 * Console command - Show.
 */
public class ShowCommand implements ConsoleCommand {
	
	/** The view. */
	private View view;

	/**
	 * Instantiates a new show command.
	 *
	 * @param view the view
	 */
	public ShowCommand(View view) {
		this.view = view;
	}

	/* Run show command
	 * @see ua.khpi.prylipa05.Command#execute()
	 */
	@Override
	public void execute() {
		System.out.println("Calculate done");
		view.viewShow();

	}

	/* Get key of command
	 * @see ua.khpi.prylipa05.ConsoleCommand#getKey()
	 */
	@Override
	public int getKey() {
		return 2;
	}

}
