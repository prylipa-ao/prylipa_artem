package ua.khpi.prylipa05;

import ua.khpi.prylipa03.View;

/**
 * Console command - Sort.
 */
public class SortCommand implements ConsoleCommand {
	
	/** The view. */
	private View view;

	/**
	 * Instantiates a new sort command.
	 *
	 * @param view the view
	 */
	public SortCommand(View view) {
		this.view = view;
	}

	/* Run sort command
	 * @see ua.khpi.prylipa05.Command#execute()
	 */
	@Override
	public void execute() {
		System.out.println("Sort done");
		view.viewSort();

	}

	/* Get key of command
	 * @see ua.khpi.prylipa05.ConsoleCommand#getKey()
	 */
	@Override
	public int getKey() {
		return 5;
	}

}
