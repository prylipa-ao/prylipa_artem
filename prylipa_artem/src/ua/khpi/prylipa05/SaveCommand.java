package ua.khpi.prylipa05;

import java.io.IOException;

import ua.khpi.prylipa03.View;

/**
 * Console command - Save.
 */
public class SaveCommand implements ConsoleCommand {
	
	/** The view. */
	private View view;

	/**
	 * Instantiates a new save command.
	 *
	 * @param view the view
	 */
	public SaveCommand(View view) {
		this.view = view;
	}

	/* Run save command
	 * @see ua.khpi.prylipa05.Command#execute()
	 */
	@Override
	public void execute() {
		System.out.println("Save done.");
		try {
			view.viewSave();
		} catch (IOException e) {
			System.out.println("Serialization error: " + e);
		}
		view.viewShow();

	}

	/* Get key of command
	 * @see ua.khpi.prylipa05.ConsoleCommand#getKey()
	 */
	@Override
	public int getKey() {
		return 3;
	}

}
