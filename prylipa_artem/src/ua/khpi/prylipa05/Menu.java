package ua.khpi.prylipa05;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Menu implements the dialogue with the user.
 */
public class Menu implements Command {

	/** Commands of menu. */
	private List<ConsoleCommand> menu = new ArrayList<ConsoleCommand>();

	/** The number of variant. */
	private int numberOfVariant;

	/**
	 * Instantiates a new menu.
	 *
	 * @param numberOfVariant
	 *            the number of task
	 */
	public Menu(int numberOfVariant) {
		this.numberOfVariant = numberOfVariant;
	}

	/**
	 * Add command to menu.
	 *
	 * @param command
	 *            the command of menu
	 * @return the console command
	 */
	public ConsoleCommand add(ConsoleCommand command) {
		menu.add(command);
		return command;
	}

	/**
	 * Choise variant menu.
	 */
	public void showCommand() {
		switch (this.numberOfVariant) {
		case 5:
			System.out.print("Random value's(1) Show(2) Save(3) Restore(4) Sort(5) Undo(6) Exit(0)\n");
			break;
		case 6:
			System.out.print("Random value's(1) Show(2) Save(3) Restore(4) Sort(5) Undo(6) RunThread(7) Exit(0)\n");
			break;
		case 7:
			System.out.print("Add(1) Delete(2) SearchMin(3) Show(4) Exit(0)\n");
		case 8:
		}

	}

	/*
	 * Run menu
	 * 
	 * @see ua.khpi.prylipa05.Command#execute()
	 */
	@Override
	public void execute() {
		showCommand();
		int choise;
		@SuppressWarnings("resource")
		Scanner in = new Scanner(System.in);
		do {
			choise = in.nextInt();
			for (ConsoleCommand command : menu) {
				if (choise == command.getKey()) {
					command.execute();
				}
			}

		} while (choise != 0);

	}

}
