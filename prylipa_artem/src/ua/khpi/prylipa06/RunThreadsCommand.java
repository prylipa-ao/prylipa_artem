package ua.khpi.prylipa06;

import java.util.concurrent.TimeUnit;

import ua.khpi.prylipa03.View;
import ua.khpi.prylipa03.ViewResult;
import ua.khpi.prylipa05.ConsoleCommand;

/**
 * Adding tasks and distributing them to parallel execution.
 */
public class RunThreadsCommand implements ConsoleCommand {

	/** The collection. */
	private View view;

	/**
	 * Init collection.
	 *
	 * @param view
	 *            the collection
	 */
	public RunThreadsCommand(View view) {
		this.setView(view);
	}

	/*
	 * Run tasks on different threads
	 * 
	 * @see ua.khpi.prylipa05.Command#execute()
	 */
	@Override
	public void execute() {
		CommandQueue commandQueue1 = new CommandQueue();
		CommandQueue commandQueue2 = new CommandQueue();
		AvgCommand avg = new AvgCommand((ViewResult) view);
		MaxCommand min = new MaxCommand((ViewResult) view);
		MinCommand max = new MinCommand((ViewResult) view);

		System.out.println("RUN THREADS");
		commandQueue1.add(avg);
		commandQueue2.add(min);
		commandQueue1.add(max);

		while (!avg.finish() && !min.finish() && !max.finish())
			try {

				TimeUnit.MILLISECONDS.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		System.out.println("FINISH");
		commandQueue1.del();
		commandQueue2.del();

	}

	/*
	 * Get key of command
	 * 
	 * @see ua.khpi.prylipa05.ConsoleCommand#getKey()
	 */
	@Override
	public int getKey() {
		return 7;
	}

	/**
	 * Gets the collection.
	 *
	 * @return the collection
	 */
	public View getView() {
		return view;
	}

	/**
	 * Sets the collection.
	 *
	 * @param view
	 *            the new collection
	 */
	public void setView(View view) {
		this.view = view;
	}

}
