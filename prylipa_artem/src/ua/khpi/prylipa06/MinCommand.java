package ua.khpi.prylipa06;

import ua.khpi.prylipa03.ViewResult;
import ua.khpi.prylipa05.Command;

/**
 * The task of finding the minimal value of the result.
 */

public class MinCommand implements Command {

	/** Collection of objects. */
	private ViewResult viewResult;
	
	/** The result. */
	private int result = 999;
	
	/** Identifier completion work. */
	private boolean finish = false;

	/**
	 * Init collection for search.
	 *
	 * @param viewResult
	 *            the collection for search
	 */
	public MinCommand(ViewResult viewResult) {
		this.viewResult = viewResult;
	}

	/**
	 * Gets the result.
	 *
	 * @return the result
	 */
	public float getResult() {
		return result;
	}

	/**
	 * Finish.
	 *
	 * @return true, if finish search
	 */
	public boolean finish() {
		return this.finish;
	}

	/* Run search
	 * @see ua.khpi.prylipa05.Command#execute()
	 */
	@Override
	public void execute() {
		System.out.println("Start search min");
		for (int i = 0; i < viewResult.getItems().size(); i++) {
			if (result > viewResult.getItems().get(i).getResult())
				result = viewResult.getItems().get(i).getResult();
		}
		finish = true;
		System.out.println("Min:" + result);
	}

}
