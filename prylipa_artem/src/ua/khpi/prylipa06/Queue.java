package ua.khpi.prylipa06;

import ua.khpi.prylipa05.Command;

/**
 * The Interface Queue - queue tasks for multi-thread execution.
 */
public interface Queue {

	/**
	 * Add task to queue.
	 *
	 * @param command
	 *            the task
	 */
	void add(Command command);

	/**
	 * Delete task from queue.
	 *
	 * @return the task
	 */
	Command del();
}
