package ua.khpi.prylipa06;

import java.util.Vector;
import ua.khpi.prylipa05.Command;

/**
 * CommandQueue - thread handler.
 */
public class CommandQueue implements Queue {

	/** The collection of tasks. */
	private Vector<Command> tasks;

	/** The flag of waiting. */
	private boolean waiting;

	/** The flag of finish. */
	private boolean finish;

	/**
	 * Instantiates a new command queue.
	 */
	public CommandQueue() {
		tasks = new Vector<Command>();
		waiting = false;
		new Thread(new Worker()).start();
	}

	/**
	 * Setting end of work.
	 */
	public void finish() {
		finish = true;
	}

	/*
	 * Add new new task to queue
	 * 
	 * @see ua.khpi.prylipa06.Queue#add(ua.khpi.prylipa05.Command)
	 */
	@Override
	public void add(Command command) {
		tasks.add(command);
		if (waiting) {
			synchronized (this) {
				notifyAll();
			}
		}
	}

	/*
	 * Remove task from queue
	 * 
	 * @see ua.khpi.prylipa06.Queue#del()
	 */
	@Override
	public Command del() {
		if (tasks.isEmpty()) {
			synchronized (this) {
				waiting = true;
				try {
					wait();
				} catch (InterruptedException e) {
					waiting = false;
				}
			}
		}
		return (Command) tasks.remove(0);
	}

	/**
	 * Worker - maintains a queue of tasks.
	 */
	class Worker implements Runnable {

		/*
		 * Run tasks
		 * 
		 * @see java.lang.Runnable#run()
		 */
		@Override
		public void run() {
			while (!finish) {
				Command command = del();
				command.execute();
			}

		}

	}
}
