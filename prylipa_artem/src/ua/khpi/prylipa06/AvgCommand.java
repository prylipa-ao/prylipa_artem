package ua.khpi.prylipa06;

import ua.khpi.prylipa02.Calc;
import ua.khpi.prylipa03.ViewResult;
import ua.khpi.prylipa05.Command;

/**
 * The task of finding the average value of the result.
 */
public class AvgCommand implements Command {

	/** Collection of objects. */
	private ViewResult viewResult;

	/** The result. */
	private float result;

	/** Identifier completion work. */
	private boolean finish = false;

	/**
	 * Init collection for search.
	 *
	 * @param viewResult
	 *            the collection for search
	 */
	public AvgCommand(ViewResult viewResult) {
		this.viewResult = viewResult;
	}

	/**
	 * Gets the result.
	 *
	 * @return the result
	 */
	public float getResult() {
		return result;
	}

	/**
	 * Finish.
	 *
	 * @return true, if finish search
	 */
	public boolean finish() {
		return this.finish;
	}

	/*
	 * Run search
	 * 
	 * @see ua.khpi.prylipa05.Command#execute()
	 */
	@Override
	public void execute() {
		System.out.println("Start search avg");
		for (Calc calc : viewResult.getItems()) {
			result += calc.getResult();
		}
		result /= viewResult.getItems().size();
		finish = true;
		System.out.println("Avg:" + result);
	}

}
