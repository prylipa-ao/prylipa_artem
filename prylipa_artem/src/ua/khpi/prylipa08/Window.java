package ua.khpi.prylipa08;

import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import ua.khpi.prylipa02.Calc;
import ua.khpi.prylipa03.ViewResult;

/**
 * Window - paint the contents window.
 */
public class Window extends Frame {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The collection viewable objects. */
	private ViewResult view;

	/**
	 * Instantiates a new window.
	 *
	 * @param view
	 *            the new view
	 */
	public Window(ViewResult view) {
		this.view = view;
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent event) {
				setVisible(false);
			}

		});
	}

	/*
	 * Paint result and graphics
	 * 
	 * @see java.awt.Window#paint(java.awt.Graphics)
	 */
	@Override
	public void paint(Graphics g) {

		g.setColor(Color.GRAY);
		view.viewInit();
		int size = view.getItems().size();
		g.drawString("E=mgh", 300, 50);

		for (int i = 0; i < size; i++) {
			Calc tmp = view.getItems().get(i);
			g.setColor(Color.BLACK);
			g.drawString(i + 1 + ". " + tmp.toString(), 30, 75 + i * 20);
			g.setColor(Color.GREEN);
			g.drawString(i + 1 + "", 180, (95 + size * 20 + i * 20));
			g.drawString(tmp.getResult() + "", (210 + tmp.calc() * 20), (115 + 2 * size * 20));
			g.setColor(Color.GRAY);
			g.drawLine(210, (95 + size * 20 + i * 20), (210 + tmp.calc() * 20), (95 + size * 20 + i * 20));

		}

		g.setColor(Color.GREEN);
		g.drawString("�", 205, (75 + size * 20));
		g.drawLine(200, (75 + size * 20), 200, (95 + 2 * size * 20));
		g.drawString("result", 360, (95 + 2 * size * 20));
		g.drawLine(200, (95 + 2 * size * 20), 350, (95 + 2 * size * 20));

	}
}
