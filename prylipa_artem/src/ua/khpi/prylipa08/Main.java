package ua.khpi.prylipa08;

import ua.khpi.prylipa05.Application;

// TODO: Auto-generated Javadoc
/**
 * The Class Main.
 */
public class Main {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		Application app = new Application(new ViewableWindow().getView());
		app = Application.getInstance();
		app.run();
	}

}
