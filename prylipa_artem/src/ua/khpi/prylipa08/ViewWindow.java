package ua.khpi.prylipa08;

import java.awt.Dimension;
import ua.khpi.prylipa03.ViewResult;

/**
 * The Class ViewWindow print result and draw graphics.
 */

public class ViewWindow extends ViewResult {

	/** The window. */
	private Window window = null;

	/**
	 * Instantiates a new view window.
	 */
	public ViewWindow() {
		super();
		window = new Window(this);
		window.setSize(new Dimension(640, 480));
		window.setTitle("Info");
		window.setVisible(true);
	}

	/*
	 * Paint result
	 * 
	 * @see ua.khpi.prylipa03.ViewResult#viewShow()
	 */
	@Override
	public void viewShow() {
		super.viewShow();
		window.setVisible(true);
		window.repaint();
	}
}
