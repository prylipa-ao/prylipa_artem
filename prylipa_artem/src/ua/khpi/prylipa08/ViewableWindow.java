package ua.khpi.prylipa08;

import ua.khpi.prylipa03.View;
import ua.khpi.prylipa03.Viewable;


/**
 * The Class ViewableWindow - design pattern (Factory Method).
 */

public class ViewableWindow implements Viewable {

	/*
	 * Creates a display object
	 */
	@Override
	public View getView() {
		return new ViewWindow();
	}

}
